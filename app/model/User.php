<?php
namespace app\model;
use think\Model;   // 使用thinkphp框架的Model类
 
// User模型  定义User模型继承Model类
class User extends Model {
    protected $json = ['info'];
    protected $jsonAssoc = true;
}