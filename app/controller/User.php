<?php

declare(strict_types=1);

namespace app\controller;


use think\facade\View;
use think\Request;
use app\model\User as UserModel;
use app\validate\User as UserValidate;
use think\exception\ValidateException;
use think\facade\Db;
use think\facade\Session;

class User
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */


    // dump(session('login'));     
    public function index(Request $request)
    {
        // dd( $request->param('__token__'));
        // dd($request->param());
        return View::fetch('user/index', [
            'list' => UserModel::withSearch(['id'], ['id' => request()->param('id')])->select()
        ]);
    }
    public function yanzheng($dengji)
    {
        try {
            validate(UserValidate::class)
            ->scene('token');
            
        } catch (ValidateException $e) {
            return '无效';
        }

        $user = Session::get('login');  //login是^ "2"

        $o = Db::table('user')->where('username', $user)->find();
        // dd($o['num']); //获取到了等级在num里，没有则为null
        if ($o == null || $o['num'] < $dengji) {
            return false;
        } else {
            return $o['num'];
        }
    }
    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        return View('create');
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $dengji = 1;
        if ($this->yanzheng($dengji)) {
            $data = $request->param();
            // dd($data);
            try {
                validate(UserValidate::class)
                    ->scene('edit')
                    ->batch(true)
                    ->check($data);
            } catch (ValidateException $e) {
                return view('../view/public/toast.html', [
                    'infos' => $e->getError(),
                    'url_text' => '返回注册页',
                    'url_path' => url('user/create')
                ]);
            }
        } else {
            return '等级不够';
        }
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        return view('edit', [
            'obj' => UserModel::find($id)
        ]);
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        $dengji = 2;
        if ($this->yanzheng($dengji)) {
            $data = $request->param();
            // dd($data);
            try {
                validate(UserValidate::class)
                    ->scene('edit')
                    ->batch(true)
                    ->check($data);
            } catch (ValidateException $e) {
                return view('../view/public/toast.html', [
                    'infos' => $e->getError(),
                    'url_text' => '返回首页',
                    'url_path' => url('/user/' . $id . '/edit')
                ]);
            }


            return UserModel::update($data) ? view('../view/public/toast.html', [
                'infos' => ['修改成功'],
                'url_text' => '返回首页',
                'url_path' => url('/')
            ]) : '修改失败';
        } else {
            return '等级不够';
        }
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {

        $dengji = 3;
        if ($this->yanzheng($dengji)) {
            return UserModel::destroy($id) ?
                View('../view/public/toast.html', [
                    'infos' => ['已删除'],
                    'url_text' => '返回首页',
                    'url_path' => url('/')
                ])
                : '删除失败';
        } else {
            return '等级不够';
        }
    }
}
