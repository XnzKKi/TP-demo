<?php

namespace app\controller;

use think\facade\Db;
use think\facade\Validate;
use think\Request;
use think\facade\View;
use think\Model;


class Login
{
    public function index()
    {
        return view('index');
    }
    public function check(Request $request)
    {
        // dd( $request->param('__token__'));
        $errors = [];
        $data = $request->param();
        //查询数据
        $validate = Validate::rule([
            'username' => 'unique:user,username^password'         //'unique:user,name^password'指向user数据库名字,u..和p..表示索引
        ]);

        //验证数据，验证失败时候返回ture,成功返回fales
        $a = $validate->check([
            'username' => $data['username'],
            'password' => $data['password']
        ]);
        // dd($a);

        //【判断，用户名和密码存在就是正确
        if ($a) {
            $errors[] = '用户名或者密码错误';
        }
        if (!empty($errors)) {
            return view('../view/public/toast.html', [
                'infos' => $errors,
                'url_text' => '返回登录',
                'url_path' => url('/login')
            ]);
        } else {
           
            return redirect('/');
        }
    }
    public function out()
    {
        session('login', null);
        return redirect('/login');
    }
}


//
// $uid = $_POST['username'];//接收前台的uid
// $pwd = $_POST['password'];//接收用户的密码



// // dump($uid,$pwd);

// $a = Db::table('user')->where('username', $uid)->find();
// // dump($a);
// if ($a==null) {
//     return '用户名不正确';
// }
// else{
//     if ($pwd === $a['password']) {
//         dump($uid,$pwd);
//         return '密码正确';
//         setcookie("usercookie", "runoob", time()+3600);
//         dump($_COOKIE['usercookie']);

//     }else{
//         return '密码错误';
//     }
// }
