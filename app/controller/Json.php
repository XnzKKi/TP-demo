<?php
namespace app\controller;

use think\facade\View;

use app\model\User as UserModel;
use think\facade\Db;
use think\Request;

class Json
{
    
    public function Index()
    {
        return view('index');
    }
    public function json(Request $request)
    {
        $json = file_get_contents('../json/data.json');

        dump( $request->param());
        //Ajax传过来的参数，page是页码，listnum是每页展示的数据条数
        $page = $request->param('page');
        $listNum = $request->param('listNum');
        //这个函数蛮重要的json_decode（）；因为实际上$json我们定义的是个String ,这个函数将它转化成json格式的数据
        $jsondata=json_decode($json);
        //定义头文件，防止乱码
        header("Content-Type: text/html; charset=UTF-8");
        //print_r($jsondata);
        //这样我们就可以拿到我们要的数组了
        $arr = $jsondata->data; 
        //echo $arr;
        //下面是为了拆分后台数据所做的努力了：$sign是判断总数据可以分成多少页
        $sign = intval(count($arr)/$listNum)+1;
        //如果传过来的页码大于总页码，不好意思，智能取模了，这样才能一直循环下去
        if ($page >= $sign) {
            $page = $page % $sign;
        }
        //array_slice(array,start,length),php截取数组的方法
        $toget = array_slice($arr,$page*$listNum,$listNum);
        //返回数据
        echo json_encode($toget); 
    }
    
}
