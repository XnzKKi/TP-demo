<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class User extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'username' => 'require|unique:user',
        '__token__' => 'require|token',
        'password' => 'require',
        'num' => 'require|max:1|number'

    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [];

    //这里是场景验证
    protected $scene = [
        'insert'  => ['username','password','num','__token__'],
        'edit' => ['username','password','num','__token__'],
        'token' => ['usernmae','__token__']
    ];
}
