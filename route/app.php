<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

// 'with_route' => true;

Route::get('think', function () {
    return 'hello,ThinkPHP6!';
});
// Route::get('hello/:name', 'index/hello');

//后台组
Route::group(function ()
{
    //用户模块路由
    Route::resource('user',"User");

})->middleware(function($request, Closure $next){
    if (!session('?login')) {
        return redirect('/login');
    }
    return $next($request);
});

//登录路由，如果已经登录了直接跳转到界面
Route::get('login',"Login/index")
->middleware(function($request, Closure $next){
    if (session('?login')) {
        return redirect('/');
    }
    return $next($request);
});
//登录检查
Route::post('login_check',"Login/check");
Route::post('json',"Json/json");
//登出路由
Route::get('logout',"Login/out");


